# JavaScript Assignment Jul 20
## Azhar reviews
* As far as the assignment output is concerned, you've a full score on that. Good work!

### Improvements
* Would recommend following indentation before committing the code.
* Code looks messy like you said, you can use a file to load script into your html for better readability.
* Use head tags to only load scripts and style sheets. Do not add Body elements like Headers and Paragraph
* Follow naming conventions on variables making it more readable.